trigger ContactFamilyCount on Contact (after insert, after update, after delete, after undelete) {
    if(trigger.isInsert || trigger.isUndelete ){ 
        
        ContactFamilyCount.after_insert(trigger.new);
        
    }
    if(trigger.isUpdate ){ 
        
        ContactFamilyCount.after_update(trigger.new,trigger.oldMap);
        
    }
    if(trigger.isDelete ){ 
        
        ContactFamilyCount.after_delete(trigger.old);
        
    }
}