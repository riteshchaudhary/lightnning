({
    getSearchValue : function(component, event, helper) {
        var eventParam = $A.get('e.c:searchComponentEvent');
        eventParam.setParams({
            "lastName" : component.find("lastName").get("v.value"),
            "firstName" : component.find("firstName").get("v.value"),
            "phone" : component.find("phone").get("v.value"),
            "email" : component.find("email").get("v.value")
        });
        eventParam.fire();
    },
})