({
    jsLoaded : function(component, event, helper) {},
    
    doInt : function(component, event, helper) {
		$("#table1 .childgrid tr").draggable({
              helper: function(){
                  var selected = $('.childgrid tr.selectedRow');
                if (selected.length === 0) {
                  selected = $(this).addClass('selectedRow');
                }
                var container = $('<div/>').attr('id', 'draggingContainer');
            container.append(selected.clone().removeClass("selectedRow"));
            return container;
              }
         });
        
        $("#table1 .childgrid").droppable({
            drop: function (event, ui) {
            $(this).append(ui.helper.children());
            $('.selectedRow').remove();
            }
        });
        
        $(document).on("click", ".childgrid tr", function () {
            $(this).toggleClass("selectedRow");
        });
	},
     doInit : function(component, event, helper) {
        var recID = component.get("v.recordId");
        var action = component.get("c.getAccountContacts");
        action.setParams({
            recordId: recID
        });
        action.setCallback(this, function(response){
            var data = response.getReturnValue();
            component.set("v.contactList", data);
        });
        $A.enqueueAction(action);
    }
})