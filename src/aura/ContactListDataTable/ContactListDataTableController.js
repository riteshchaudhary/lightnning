({
	doInit : function(component, event, helper) {
         var contactRecord = component.get("c.getContactList");
        contactRecord.setCallback(this, $A.getCallback(function (status) {
            var record = status.getState();
            if(record === 'SUCCESS'){
                component.set("v.contact_Object",status.getReturnValue());
            }else{
                var messageEvents = $A.get("e.force:showToast");
                messageEvents.setParams({
                    "title": "Error!",
                    "message": " Something has gone wrong."
                });
                messageEvents.fire();
            }
        }));
        $A.enqueueAction(contactRecord);
		
	}
})