({
    doInit : function(component, event, helper) {
        helper.handelDoInit(component, event, helper);
    },
    
    isSearchList : function(component, event, helper){
        var applicationParam = event.getParams();
        component.set("v.appPram",applicationParam);
        var appParam = component.get("v.appPram");
        helper.handelisSearchList(component, appParam, null);
    },
    
    onSort: function(component, event, helper){
        var order = component.get("v.sortOrder");
        var sortString  = event.getSource().getLocalId();
        
        if(order === 'ASC'){
            sortString += '-ASC';
            component.set("v.sortOrder","DESC");
        }else{
            sortString += '-DESC';
            component.set("v.sortOrder","ASC");
        }
        
        var appParam = component.get("v.appPram");
        if(appParam == null){
            var applicationParam = event.getParams();
            component.set("v.appPram",applicationParam);
        }
        
        appParam = component.get("v.appPram");
        
        helper.handelisSearchList(component, appParam, sortString);
    },
    
})