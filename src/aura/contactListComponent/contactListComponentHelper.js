({
    handelDoInit : function(component, event, helper) {
        var contactRecord = component.get("c.getContactList");
        contactRecord.setCallback(this, $A.getCallback(function (status) {
            var record = status.getState();
            if(record === 'SUCCESS'){
                var objectA = status.getReturnValue();
                component.set("v.Contact",status.getReturnValue());
                for(var headInfor in objectA){
                    if(Object.keys(objectA[headInfor]) != null){
                        component.set("v.headRow",Object.keys(objectA[headInfor]));
                        console.log(JSON.stringify(component.get("v.headRow")));
                    }                    
                }
                this.handelpagination(component, event,objectA);
            }else{
                var messageEvents = $A.get("e.force:showToast");
                messageEvents.setParams({
                    "title": "Error!",
                    "message": " Something has gone wrong."
                });
                messageEvents.fire();
            }
        }));
        $A.enqueueAction(contactRecord);
    },
    
    handelisSearchList : function(component, applicationParam, param) {       
        var newContactList = component.get("c.getReloadList");
        newContactList.setParams({
            "lastName" : applicationParam.lastName,
            "firstName" : applicationParam.firstName,
            "phone" : applicationParam.phone,
            "email" : applicationParam.email,
            "sortStr":param
        });
        newContactList.setCallback(this,function(a){
            if(a.getState() === 'SUCCESS'){
                component.set("v.Contact",a.getReturnValue());   
            } 
        });        
        $A.enqueueAction(newContactList);
    },
    
    handelpagination : function(component, event,objectRecord){
        var pageSize = 5;
        var nextPage = 5;
        var prevPage = 0;
        var count=0;
        count = objectRecord.length / pageSize;
        for(var i =1 ; i <= count ; i++){
            $A.createComponent(
                "lightning:button",
                {
                    "aura:id": "page"+i,
                    "label": i,
                },
                function(newButton, status, errorMessage){
                    if (status === "SUCCESS") {
                        var body = component.get("v.body");
                        body.push(newButton);
                        component.set("v.body", body);
                    }
                }
            );
        }
        
    }
})