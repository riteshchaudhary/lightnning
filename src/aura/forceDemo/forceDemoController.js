({
    doInit: function(component, event, helper) {
        component.find("forceRecord").getNewRecord(
            "Contact",
            null,
            false,
            $A.getCallback(function() {
                var record = component.get("v.simpleRecord");
                console.log(record);
                var error = component.get("v.errorRecord");
                if(error || (record === null)) {
                    console.log("Error initializing record template: " + error);
                }
                else {
                    console.log("Record template initialized: " + record.sobjectType);
                }
            })
        );
        
    }
})