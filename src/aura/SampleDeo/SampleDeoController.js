({
    doInit : function(component, event, helper) {
        helper.getData(component);
        $A.createComponents([
            ["aura:text",{value:'Hello'}],
            ["ui:button",{label:'Button'}],
            ["aura:text",{value:'World'}]
        ],function(components,status,statusMessagesList){
            // Components is an array of 3 components
            // 0 - Text Component containing Hello
            // 1 - Button Component with label Button
            // 2 - Text component containing World
        });
        
    }
})