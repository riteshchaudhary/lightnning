({
    doInit : function(component, event, helper) {
        helper.doInitHelper(component,event,helper);
        var pickValue = component.get("c.getCleanStatus"); 
        pickValue.setParams({
            "objObject" :"{sobjectType : 'Contact'}",
            "fld" : "CleanStatus"
        });
        pickValue.setCallback(this,function(a){
            if(a.getState() === "SUCCESS"){
                component.set("v.optionValue",a.getReturnValue());
            }
        });
        $A.enqueueAction(pickValue);
    },
    
    saveContact:function(component, event, helper) {
        helper.handleSaveContact(component);
    },
    
    onChange:function(component, event, helper) {
        helper.handelonChange(component);
    },
    
    cancel: function(component, event, helper) {
        if(! component.get("v.isEditData")){
            var homeEvent = $A.get("e.force:navigateToObjectHome");
            homeEvent.setParams({
                "scope": "Contact"
            });
            homeEvent.fire();
        }else{
            var detailEvent = $A.get("e.force:navigateToSObject");
            detailEvent.setParams({
                "recordId": component.get("v.simpleRecord.Id")  ,
                "slideDevName": "detail"
            });
            detailEvent.fire();
        }
        
    }, 
    
    recordUpdated :function(component, event, helper){
        helper.handleRecordUpdated(component, event);
        
    }
})