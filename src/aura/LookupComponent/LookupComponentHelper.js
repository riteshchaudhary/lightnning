({
    doInitHelper: function(component, event, helper) {
        component.find("forceRecord").getNewRecord(
            "Contact",
            this.getUlrParam("recordTypeId"),
            false,
            $A.getCallback(function() {
                var record = component.get("v.simpleRecord");
                var error = component.get("v.errorRecord");
                if(error || (record === null)) {
                    console.log("Error initializing record template: " + error);
                }
                else {
                    console.log("==Default=Loaded==="+JSON.stringify(record));
                    console.log(component.get("v.isEditData")); 
                    if(component.get("v.isEditData")){
                        component.set("v.simpleRecord",record);
                        component.find("forceRecord").reloadRecord();
                    }
                }
            })
        );
    },
    
    handleSaveContact: function(component){
        var allValid = component.find('field').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if (allValid) {
            var saveFlag = component.get("v.showFields");
            if(!saveFlag ){
                var contact = component.get("v.simpleRecord");
                console.log(contact);
                contact.Birthdate=null;
                contact.Blood_Group__c=null;
                contact.MailingCity=null;
                contact.Status__c = null;
                component.set("v.simpleRecord",contact);    
            }
            component.find("forceRecord").saveRecord(function(saveResult) {
                console.log("=========="+saveResult.state);
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {               
                    var detailEvent = $A.get("e.force:navigateToSObject");
                    detailEvent.setParams({
                        "recordId": component.get("v.simpleRecord.Id")  ,
                        "slideDevName": "detail"
                    });
                    detailEvent.fire();
                }                    
            }); 
        } 
    },
    
    handelonChange : function(component){
        var isCheck = component.find("checkbox").get("v.checked");
        component.set("v.showFields",isCheck);
        var contact = component.get("v.simpleRecord");
        if(! isCheck){
            if(contact != null){
                if((contact.Birthdate != null) || (contact.Blood_Group__c != null) || (contact.MailingCity != null) || (contact.Status__c != null)){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Warning",
                        "message": "If you uncheck then Additional information will not changes will not save and fill with null values."
                    });
                    resultsToast.fire();
                }
            }
        }
    },
    
    handleRecordUpdated : function(component, event){ 
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var contact = component.get("v.simpleRecord");       
            if(contact != null){
                component.set("v.simpleRecord",contact);
                console.log("=========Loaded==="+JSON.stringify(contact));
                if(contact.Id != null){
                    if((contact.Birthdate != null) || (contact.Blood_Group__c != null) || (contact.MailingCity != null) || (contact.Status__c != null)){
                        component.find("checkbox").set("v.checked",true);
                        component.set("v.showFields",true);
                    }else{
                        component.find("checkbox").set("v.checked",false);
                        component.set("v.showFields",false);
                    }
                    component.set("v.isEditData",true);
                }
            }else{
                component.set("v.simpleRecord",contact);
                component.set("v.isEditData",false);  
            }
        }else if(eventParams.changeType === "CHANGED") {
            component.find("forceRecord").reloadRecord();
        }  
    },
    
    getUlrParam: function(param){
        param = param.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+param+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if( results == null )
            return null;
        else
            return results[1];
    },
    
})