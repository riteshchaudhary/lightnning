({
    loadScript : function(component, event, helper) {},
    doInit : function(component, event, helper) {
        var action = component.get('c.getcontact');
        console.log(action);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var myData = response.getReturnValue();
                component.set('v.contacts', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
    },
    
    allowDrop: function(cmp, event, helper){
        event.preventDefault();
    },
    
    drag: function(cmp, ev, helper){
        var parentId = document.getElementById(ev.target.id).parentElement.id;
        cmp.set("v.startId",ev.target.id);
        cmp.set("v.parentId",parentId);
    },
    
    drop: function(cmp, ev, helper){
        var drag = cmp.get("v.startId");
        var div = ev.target.id;
        var fragment = document.createDocumentFragment();
        fragment.appendChild(document.getElementById(drag));
        document.getElementById(div).appendChild(fragment);
        var c = document.getElementById(div).children;
        var x = document.getElementById('drag1').parentElement.id;
        var fragment = document.createDocumentFragment();
        fragment.appendChild(document.getElementById(c[0].id));
        document.getElementById(cmp.get("v.parentId")).appendChild(fragment);
    },
    logChange1: function(component, event, helper) {
        console.log(component.get("v.box1"));
    },
    logChange2: function(component, event, helper) {
        console.log(component.get("v.box2"));
    },
    
})