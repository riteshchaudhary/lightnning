public class ConcatNameHandler extends TriggerHandler {

    public override void beforeInsert() {
       
        Map <Contact, Account> associateMap  = new Map<Contact, Account>();
        
        for(Contact contact_obj : (List<Contact>) Trigger.new ){           
            associateMap.put(contact_obj, new Account(Name = contact_obj.FirstName +'-'+contact_obj.lastName));
        }
        insert associateMap.values();
        
        for(Contact temp_obj : associateMap.keySet()){
            temp_obj.AccountId = associateMap.get(temp_obj).id;
        }
    } 
    
    public override void beforeUpdate() {
        List<Account> accountList = new List<Account>();
        for(Contact contact_obj : (List<Contact>) Trigger.new ){  
            if(contact_obj.AccountId != null){
               
                Contact Temp_contact = (Contact) Trigger.oldMap.get(contact_obj.Id);
                               
                if(contact_obj.LastName != Temp_contact.LastName || contact_obj.FirstName != Temp_contact.FirstName ){
                 accountList.add(new Account(id=contact_obj.AccountId, Name = contact_obj.FirstName +'-'+contact_obj.lastName ));  
                }
                
            }
        }
        
        if(accountList.size() > 0){
            update accountList;
        }
    } 
}