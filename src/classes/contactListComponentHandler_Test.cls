@isTest
public class contactListComponentHandler_Test {
    @TestSetup
    public static void testMyFunction(){
        List<Contact> contactList = new List <Contact>();
        for(Integer i =1 ; i < 11 ;i++){
            contactList.add(new Contact(LastName='LastName'+i, FirstName='FirstName'+i, Phone='987456321'+i, Email='email'+i+'@test.com'));
        }
        if(contactList != null){
            Insert contactList;
        }
       
        
    }
    @isTest
    public static void getContactListTest(){
        Test.startTest();
        List <Contact>conList = contactListComponentHandler.getContactList();
        System.assertEquals(10, conList.size());        
        Test.stopTest();
        
    }
    
    @isTest
    public static void getReloadList(){
        Test.startTest();
        List <Contact>conList = contactListComponentHandler.getReloadList('lastName1', 'firstName2', '9874563213', 'email4@test.com', null);
        System.assertEquals(5, conList.size());        
        
        List <Contact>conList1 = contactListComponentHandler.getReloadList('lastName1', 'firstName2', '9874563213', 'email4@test.com', 'FirstName-DESC');
        System.assertEquals(5, conList1.size());
        
        List <Contact>conList2 = contactListComponentHandler.getReloadList('lastName1', 'firstName2', '9874563213', 'email4@test.com', 'Phone-ASC');
        System.assertEquals(5, conList2.size());
        
         List <Contact>conList3 = contactListComponentHandler.getReloadList('', '', '', '', 'FirstName-DESC');
        System.assertEquals(10, conList3.size());
        
        List <Contact>conList4 = contactListComponentHandler.getReloadList('', '', '', '', 'FirstName-ASC');
        System.assertEquals(10, conList4.size());
        
        Test.stopTest();
        
    }
}