public class ContactFamilyCount{
    public static void after_insert(List<Contact> conlist){ 
        
        Set<id> setOfAccIds = new Set<id>();    
        for(Contact con : conlist){
            
            setOfAccIds.add(con.AccountId);
            
        }
        familycount_operation(setOfAccIds);
        
        
        
    }
    public static void after_update(List<Contact> conlist,Map<id,Contact> oldMap){ 
        
        Set<id> setOfAccIds = new Set<id>();    
        for(Contact con : conlist){
            if(con.AccountId != null && con.accountid != oldMap.get(con.Id).AccountId){
                setOfAccIds.add(oldMap.get(con.Id).AccountId);
                setOfAccIds.add(con.AccountId);
            }
        }
        familycount_operation(setOfAccIds);  
        
        
        
    }
    public static void after_delete(List<Contact> conlist){ 
        
        Set<id> setOfAccIds = new Set<id>();    
        for(Contact con : conlist){
            
            setOfAccIds.add(con.AccountId);
            
        }
        familycount_operation(setOfAccIds);  
        
    }
    public static  void familycount_operation(Set<id> setOfAccIds){
        
        
        Map<Id, Integer> countMap = new Map<Id, Integer>();        
        for (AggregateResult ar : [SELECT AccountId AcctId, Count(id) ContactCount 
                                   FROM Contact 
                                   WHERE AccountId in: setOfAccIds
                                   GROUP BY AccountId]){
                                       countMap.put(''+ar.get('AcctId'),(Integer)(ar.get('ContactCount')));
                                       
                                   }
        system.debug(countMap +'Map value');
        List<Contact> contacts = [Select Id,TotalFamilyMember__c,AccountId from Contact where AccountId IN:setOfAccIds];
        system.debug(contacts +'connnnnnnn');
        if(! contacts.isEmpty()){
            for(Contact c: contacts ){
                c.TotalFamilyMember__c = ''+countMap.get(c.AccountId);
                //system.debug(totalContactCount +'family count');
               // c.TotalFamilyMember__c = totalContactCount;
                system.debug(c.TotalFamilyMember__c +'total family count');
            }
            update contacts;
            
            
        }
    }
    
}