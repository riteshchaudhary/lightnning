public with sharing class lookComponentController {
    public Project__c project{get;set;}
    public List<sobject> projectList{get;set;}
    public lookComponentController(){
        projectList = new List<Project__c>([Select id, name from Project__c]);
    }
}