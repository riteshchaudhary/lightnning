public Class ContactTestFactory {
    
    /*
        This method take Two param as String and return Contact record 
        that having LastName and FirstName. 
    */
    public static Contact insertContact( String Lname , String Fname){
        
        return new Contact(LastName= Lname , FirstName = Fname); //return contact record

    }
  
}