public with sharing class ProjectController {
   
   public Project__c projectCon {get;set;}
   Public Id proID {get;set;}
   /*------------- Project Task---------------------------*/
   public list<Project_Task__c> taskList{get;set;}
   public list<WrapProjectTask> task_Wrap {get;set;} 
   public Integer counter{get;set;}

   public ProjectController (){
        projectCon = new Project__c();
        
        counter = 0;
        taskList = new List<Project_Task__c>();
        task_Wrap = new List<WrapProjectTask>();
    

   }
   
     /*Adding tasl in tasklist*/
     public PageReference addRow(){
        
        WrapProjectTask projectTsk = new WrapProjectTask(new Project_Task__c(),proID);
        counter++;
        projectTsk.counterWrap = counter; 
        task_Wrap.add(projectTsk); 
        return null;    
    }
    
    /*Removing task in taskList*/
    public PageReference removingRow(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        
        for(Integer i=0;i<task_Wrap.size();i++){
            if(task_Wrap[i].counterWrap == param ){
                task_Wrap.remove(i);     
            }
        }
        counter--;
        return null;    
    }

    //save Project task in custome sobject
    public PageReference bulksave(){
    
            list<Project_Task__c> updateTaskList = new list<Project_Task__c>();
            if(!task_Wrap.isEmpty() && task_Wrap != null){
                for(WrapProjectTask wpt : task_Wrap){
                     updateTaskList.add(wpt.pro_task);
                }
            }
            System.debug('==========');
            if(!updateTaskList.isEmpty() && updateTaskList != null){
                upsert(updateTaskList);
            }
            return null;
    }

    // Save Project inbformation in custome sobject
    public PageReference save(){
        insert projectCon;
        proID = projectCon.id;
        System.debug('========Project ID======= : '+ proID);
        return null;
    }  
  
  /*--------------WrapProjectTask-------------------------*/
    public class WrapProjectTask{
        public Project_Task__c pro_task {get;set;}
        public Integer counterWrap{get;set;}
        
        public WrapProjectTask(){
            
        }
        
        public WrapProjectTask( Project_Task__c pt){
            this.pro_task = pt;
        }
        
        public WrapProjectTask( Project_Task__c pt, ID projectid  ){
            this.pro_task = pt;
            this.pro_task.Projectid__c = projectid;
        }
    }
  /*---------------------------------------------------------*/
}//end class