public class TimeSheetDetailController {
        
   //object of timesheet_detail_.........
   public Timesheet_Detail__c Timesheet_Detail {get;set;}

   //List of project object........
   public List <Project__c> projectList{get;set;}
   public List <SelectOption> proList{get;set;}

   //List of Task object...........
   public List<TaskWrap> taskwrapList{get;set;}
   public List <SelectOption> taskList{get;set;}
   public map<id,String> taskNameMap{get;set;}
   //public String JsonMap{get;set;} 
   public string proId{get;set;}
   public id taskId{get;set;}

     
   // list of task related to projects 
   public void Tasknamelist(){
        taskList = new List<SelectOption>();
        taskNameMap = new Map<id,String>();
        if(proId != null){
            for(Project_Task__c t : Database.query('Select id, Projectid__c, name from Project_task__c where Projectid__c =: proID')){
              taskList.add(new SelectOption(t.id, t.name));
              taskNameMap.put(t.id,t.name);
               
            }
         }
         System.debug('================taskList======'+taskList);
         System.debug('================taskNameMap======'+taskNameMap);
   }
   
   //map with project id and task list
   public TimeSheetDetailController (){
      
      Timesheet_Detail = new Timesheet_Detail__c();
      projectList = new List<Project__c>([Select id , name from project__c]);
      System.debug('========ProjectList====='+projectList);

      proList = new List<SelectOption>();
         if(!projectList.isEmpty() && projectList != null){
            for(Project__c p : projectList){
              proList.add(new SelectOption(p.id, p.name));
            }
          }
         System.debug('==========TaskList======='+proList);
   }
   
    public PageReference save(){
     if(Timesheet_Detail.task_Name__c != null){
         if(taskNameMap.containsKey(Timesheet_Detail.task_Name__c)){
             Timesheet_Detail.task_Name__c =    taskNameMap.get(Timesheet_Detail.task_Name__c) ;
         }
     }
     insert Timesheet_Detail;
     return null;
  } 
  
  /*-------------------Wrap class-------------------------*/
      public class TaskWrap{
        public Timesheet_Detail__c timesheet{get;set;}
        public TaskWrap(){}
        public TaskWrap(Id projectid, Timesheet_Detail__c timesheet ){
            this.timesheet = timesheet;
            this.timesheet.Projectid__c = projectid;    
        }
      }
  /*-------------------end wrap class----------------------*/ 
}