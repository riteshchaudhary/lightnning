public class kanbanWrap{
        @AuraEnabled
        List<sObject> records {get;set;}
        @AuraEnabled
        List<String> pickVals {get;set;}
        public kanbanWrap(List<sObject> recs, List<String> pVals){
            this.records = recs;
            this.pickVals = pVals;
        }
    }