public class StudentsData {
    
    @AuraEnabled
    public Static  list<Contact> getStudents(){
        return [select id,LastName,FirstName from Contact ];
       // return null;
    }
    
    
     @AuraEnabled
    public Static  list<Contact> sort_Students(list<Contact> StudentsData){
        System.debug('StudentsData '+StudentsData);
          update StudentsData;
        return StudentsData;
    }

}