@isTest
public class contactTriggerTest {
	
   @testSetup    
    public static void testSetupMethod(){
      
        Contact con = ContactTriggerTestHandler.InsertContact('FirstNameTest', 'LastNameTest');
        Test.startTest();
        insert con;
        
        
        Contact conTemp = [select FirstName, LastName , accountId from Contact where id =: con.id limit 1];
        Account acc = [Select id, name from Account where id =: conTemp.accountId Limit 1];
        System.assertEquals(conTemp.FirstName+'-'+conTemp.LastName, acc.Name);
        Test.stopTest();
    }

    @isTest 
    public static void testBeforeUpdate(){
    	Contact con = [select FirstName, LastName , accountId from Contact limit 1]; 
        con.LastName='Change';
        con.FirstName='Change';
        Test.startTest();
        update con;
        
        Account acc = [Select id, name from Account where id =: con.accountId Limit 1];
        System.assertEquals(con.FirstName+'-'+con.LastName, acc.Name);
        
        Test.stopTest();
    }    

}