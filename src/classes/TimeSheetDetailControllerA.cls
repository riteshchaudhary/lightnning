public class TimeSheetDetailControllerA {
        
   //object of timesheet_detail_.........
   public Timesheet_Detail__c Timesheet_Detail {get;set;}

   //List of project object........
   public List <Project__c> projectList{get;set;}
   //public List <SelectOption> Task_name{get;set;}

   //List of Task object...........
   public List<TaskWrap> taskwrapList{get;set;}
   
   //public String JsonMap{get;set;} 
   public string proId{get;set;}
   public string taskId{get;set;}

   //selected options 
   public List<SelectOption> getProject_name(){
      List <SelectOption> options = new List<SelectOption>();
         if(!projectList.isEmpty() && projectList != null){
            for(Project__c p : projectList){
              options.add(new SelectOption(p.id, p.name));
            }
         }
         return options;
   }
   
   // list of task related to projects 
   public List<SelectOption> getTask_name(){
          List <SelectOption> options = new List<SelectOption>();
        if(proId != null){
            for(Project_Task__c t : Database.query('Select id, Projectid__c, name from Project_task__c where Projectid__c =: proID')){
            //for(Project_Task__c t : Database.query('Select id, Projectid__c, name from Project_task__c')){
               options.add(new SelectOption(t.id, t.name));
            }
         }
         System.debug('========='+options);
       return options;
   }
   
   //map with project id and task list
   public TimeSheetDetailControllerA (){
       Timesheet_Detail = new Timesheet_Detail__c();
     projectList = new List<Project__c>([Select id , name from project__c]);
     System.debug('========ProjectList====='+projectList);
   }
   
    public PageReference save(){
    if(proId != null){
        Timesheet_Detail.projectid__c = proId; 
    }
    
    if(taskId != null){
        //String str = taskId.split('-')[1];
        //System.debug('========str====='+srt);
        Timesheet_Detail.Task_Name__c = taskId;
    }
    System.debug('========proId====='+proId);
    System.debug('========taskId====='+taskId);
     insert Timesheet_Detail;
    
     return null;
  } 
  
  /*-------------------Wrap class-------------------------*/
      public class TaskWrap{
        public Timesheet_Detail__c timesheet{get;set;}
        public TaskWrap(){}
        public TaskWrap(Id projectid, Timesheet_Detail__c timesheet ){
            this.timesheet = timesheet;
            this.timesheet.Projectid__c = projectid;    
        }
      }
  /*-------------------end wrap class----------------------*/ 
}