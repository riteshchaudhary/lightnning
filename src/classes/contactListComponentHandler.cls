public class contactListComponentHandler {
    @AuraEnabled
    public static List<Contact> getContactList(){
        return [SELECT LastName, FirstName, Phone, Email 
                FROM Contact 
                LIMIT 1000];  
    }
    
    @AuraEnabled
    public static String getStringData(){
        List<Contact> conList = [SELECT LastName, FirstName, Phone, Email 
                FROM Contact 
                LIMIT 1000];
        System.debug('=========='+JSON.serialize(conList));
        return JSON.serialize(conList,false);
    }
    
    @AuraEnabled
    public static List<Contact> getReloadList(String lastName, String firstName, String phone, String email, String sortStr){
        String sortString ='SELECT LastName, FirstName, Phone, Email FROM Contact ';
        String whereStr = isWhere(lastName, firstName, phone, email);
        if(sortStr == '' || sortStr == null){
            if(whereStr != ''){
                whereStr = whereStr.removeEnd('OR');
                sortString += ' WHERE '+whereStr;
            }
            sortString +=' LIMIT 1000';
        }else{
            sortStr= sortStr.replace('-', ' ');
            if(whereStr != ''){
                whereStr = whereStr.removeEnd('OR');
                sortString += 'WHERE ' + whereStr;
                
            }
            sortString +=' ORDER BY '+sortStr+' NULLS LAST LIMIT 1000';
        }
        return Database.query(sortString) ;
    }
    
    public static String isWhere(String lastName, String firstName, String phone, String email){
        String whereString = '';
        if(lastName != ''){
            whereString += ' '+ bindSearch('LastName',lastName);
        }
        if(firstName != ''){
            whereString += ' '+ bindSearch('FirstName',firstName);
        }
        if(email != ''){
            whereString += ' '+ bindSearch('Email',email);
        }
        if(phone != ''){
            whereString += ' '+ bindSearch('Phone',phone);
        }
        return whereString.trim();
    }
    
    public static string bindSearch(String field, String value){
        String tempValue = '';
        if(field != null && value != null){
            tempValue = field+' LIKE \'%'+value+'%\' OR ';    
        }
        return tempValue;
    }
}