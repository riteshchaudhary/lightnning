public class timesheetHomeA{
     
     //------Project objects-----------------------
     public List <Project__c> projectList{get;set;}
     public List<ProjectWrap> projectWrap{get;set;}
     
     //-----Timesheet objects---------------------------------
     public List <Timesheet_Detail__c> dtimesheetList{get;set;}
     public List<timesheetWrap> dtimesheetWrap{get;set;}
     public DateTime currentDate{get;set;}
     public DateTime tempdate{get;set;}
     //public List <Date> taskdateList{get;set;}
     
     //--------calender events------------------
     
     public Boolean isCalenderEvent{get; set;}
     public List<calenderEvent> events{get;set;}
     public String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';

     
     //------Default constructor------------------
     public timesheethomeA(){
         projectList = new List<Project__c>([select id, Name, Customer_Name__r.Name, BIlling_Method__c, Rate_Per_Hours__c from Project__c where Customer_Name__c != null]);
        // taskdateList = new List<Date>([select Date__c  from Timesheet_Detail__c order by date__c desc]);
         // projectList = new List <Project__c>([select id, Name, Customer_Name__c, BIlling_Method__c, Rate_Per_Hours__c from Project__c]);
         dtimesheetList = new List <Timesheet_Detail__c>([select date__c, Projectid__r.name, Projectid__r.Customer_Name__c, task_Name__c, owner.name, Hours_spent__c, status_del__c, notes__c from Timesheet_Detail__c]);          
         System.debug('========'+dtimesheetList);

         //current date-------------
         currentDate = Date.today();
         System.debug('---------Curent Date--------'+currentDate);

         //----------------calender event-------------------------
         isCalenderEvent = true;
         System.debug('=========isCalenderEvent====='+isCalenderEvent);
     }
     
     
    //--------------------------Calender block---------------------------
    
     //--------Loading Calender ------
     public PageReference pageLoad(){
        events = new List<calenderEvent>();

        for( Timesheet_Detail__c td : [select id, date__c, Projectid__r.name, Projectid__r.Customer_Name__c, task_Name__c, owner.name, Hours_spent__c, status_del__c, notes__c from Timesheet_Detail__c]){
             DateTime startDT = td.date__c;  
             calenderEvent dtask = new calenderEvent();
             dtask.title= td.task_Name__c;
             dtask.startString = startDT.format(dtFormat);
             dtask.endString = td.Hours_spent__c;
             dtask.url = '/'+td.Id;
             dtask.className='event-campaign';
             events.add(dtask);
        }

        if(isCalenderEvent){

        }
        return null;

     }

     public PageReference toggleMyEvents() {
        if(isCalenderEvent){
            isCalenderEvent = false;
        }
        else{
            isCalenderEvent = true;
        }
        pageload();
        return null;
    }




    
    //-------------------------Nextmonth---------------------------------
    
    public void nextMonthview(DateTime currentdate){
        if(tempdate == null){
            tempdate = currentDate;
            tempdate =currentDate.addMonths(1);
        }else{
            tempdate =currentDate.addMonths(1);
        }
    }


   



















    //-------------------------Nextmonth---------------------------------
    
    public void prevMonthview(DateTime currentdate){
        if(tempdate == null){
            tempdate = currentDate;
            tempdate =currentDate.addMonths(-1);
        }else{
            tempdate =currentDate.addMonths(1);
        }
    }


    
    //~~~~~~~~~~~~~~~~~~~return current year~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public Integer currentYear(DateTime currentDate){
       String current_date_str = currentDate.format('yyyy-MM');
       System.debug('==============curent year'+current_date_str.split('-')[0].trim());
       return Integer.valueOf(current_date_str.split('-')[0].trim());
    }
    
    //~~~~~~~~~~~~~~~~~~~return current month~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public Integer currentMonth(DateTime currentDate){
       String current_date_str = currentDate.format('yyyy-MM');
       System.debug('==============curent month'+current_date_str.split('-')[1].trim());
       return Integer.valueOf(current_date_str.split('-')[1].trim());
    }
    
    //~~~~~~~~~~~~~~~~~ Month calender ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public void monthCalender(Integer year, Integer month){
                  
    }
    
    
    
    //--------------------------End Calender block--------------------------
    
    //~~~~~~~~~~~~~~~~~~~~~~~~Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class ProjectWrap{
        public Project__c project_WP{get;set;}
       
        //Default constructor
        public ProjectWrap(){
            
        }
        
        //constructor with two param
        public ProjectWrap(Project__c project){
            this.project_wp = project;
        }
         
    }//end timesheetWrap 
    
    //~~~~~~~~~~~~~~~~~~~~End Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    


    //~~~~~~~~~~~~~~~~~~~~~~~~calendeer Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class calenderEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
         
    }//end timesheetWrap

    

    //~~~~~~~~~~~~~~~~~~~~~~~~Timesheet Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class timesheetWrap{
        public Timesheet_Detail__c timesheetDetail{get;set;}
        
        //Default constructor
        public timesheetWrap(){
            
        }
        
        //constructor with two param
        public timesheetWrap(Timesheet_Detail__c timesheetDetail){
            this.timesheetDetail = timesheetDetail;
        }
         
    }//end timesheetWrap
    
    //~~~~~~~~~~~~~~~~~~~~End Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}//end timesheethome