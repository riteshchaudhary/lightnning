public class DetailTimesheetContorller{

    /*--------------Objects----------------------------*/
      
      //------Project objects-----------------------
      public List <Project__c> projectList{get;set;}
      public List<ProjectWrap> projectWrap{get;set;}

      //-----Timesheet objects---------------------------------
      public List <Timesheet_Detail__c> dtimesheetList{get;set;}
      public List<timesheetWrap> dtimesheetWrap{get;set;}

      //calender related objects 
      public List<CalenderData> calenderDataList{get;set;}
      public String monthOf{get;set;}
      public DateTime currentDate;
      public List<String> monthList{get;set;}
      public String prevLastDay{get;set;}
      public String curYear{get;set;}


    /*----------------End Object-----------------------*/


    /*---------------- constructor----------------------*/

      public DetailTimesheetContorller(){
        
        currentDate = date.today();
        monthOf = currentDate.format('MMM yyyy');
        loadMonth(currentDate.format('yyyy-MM'));
        
        projectList = new List<Project__c>([select id, Name, Customer_Name__r.Name, BIlling_Method__c, Rate_Per_Hours__c from Project__c where Customer_Name__c != null]);

        dtimesheetList = new List <Timesheet_Detail__c>([select date__c, Projectid__r.name, Projectid__r.Customer_Name__c, task_Name__c, owner.name, Hours_spent__c, status_del__c, notes__c from Timesheet_Detail__c]);          
        System.debug('========'+dtimesheetList);

        
      }

     /*----------------End constructor----------------------*/

    /*----------------Method related to Calender-------*/

    //----Calender data ----------------
    public void onCalenderDataLoad(){
        calenderDataList = new List<CalenderData>();
        for(Timesheet_Detail__c  td :[select id, date__c, Projectid__r.name,  task_Name__c, Hours_spent__c  from Timesheet_Detail__c where date__c != null limit 10000]){
          CalenderData tempCalenderData = new CalenderData();
          
          // map the timesheet data with clanederdata class
          tempCalenderData.timeLogId = td.id;
          DateTime task_data = td.Date__c;
          tempCalenderData.timeLogDate = task_data.format('yyyy-MM-d');
          tempCalenderData.workingTime = td.Hours_spent__c;
          tempCalenderData.projectName= td.Projectid__r.name;
          tempCalenderData.projectTaskName = td.task_Name__c;

          // adding into list
          
           calenderDataList.add(tempCalenderData);
        }//end for each loop
         
         System.debug('=====calenderDataList Size===='+calenderDataList.size());
        System.debug('=====calenderDataList Value===='+calenderDataList);
    }
    
    
    
    public void nextMonth(){
        if(currentDate != null){
            System.debug('======================Before Adding month============='+currentDate);
            currentDate =  currentDate.addMonths(1);
            monthOf = currentDate.format('MMM yyyy');
            System.debug('======================After Adding  month============='+currentDate);
             loadMonth(currentDate.format('yyyy-MM'));
            
        }
    }
    
    public void prevMonth(){
        if(currentDate != null){
            System.debug('======================Before Removing month============='+currentDate);
            currentDate =  currentDate.addMonths(-1);
            monthOf = currentDate.format('MMM yyyy');
            System.debug('======================After Removing  month============='+currentDate);
            loadMonth(currentDate.format('yyyy-MM'));
            
        }
    }
    
    public void currentMonth(){
        if(currentDate != null){
            System.debug('======================Before  month============='+currentDate);
            currentDate = date.today();
            monthOf =  currentDate.format('MMM-yyyy');
            System.debug('======================After   month============='+currentDate);
            loadMonth(currentDate.format('yyyy-MM'));
           
            
        }
    }

    public void loadMonth(String cDate){
         System.debug('======================loading Data=============');
         monthList = New List<String>();
       
      Integer cYear = Integer.valueOf(cDate.split('-')[0]);
      Integer cMonth = Integer.valueOf(cDate.split('-')[1]);
      Integer numberOfDays = date.daysInMonth(cYear,cMonth);
      DateTime tempday = DateTime.newInstance(cYear,cMonth,1);
      tempday = tempday.addDays(-1);
      prevLastDay = tempday.format('E');
      curYear = cDate;
      
      System.debug('=======Year==='+ cYear +' month is =='+cMonth +'no of days==='+numberOfDays);
      for(Integer i = 1 ; i <= numberOfDays;i++){
        DateTime dt = DateTime.newInstance(cYear,cMonth,i);
        monthList.add(dt.format('yyyy-MM-d'));
      }

      System.debug('======monthList ==='+monthList.size());
      System.debug('=========monthList===='+monthList);

    }

    /*----------------End Method related to Calender-------*/

    /*--------------------Nested Class-----------------*/

    /*----------Calender Data class--------------------*/
    public class CalenderData{
        public String timeLogId{get;set;}
        public String timeLogDate{get;set;}
        public String workingTime{get;set;}
        public String projectName{get;set;}
        public String projectTaskName{get;set;}
    }
    //~~~~~~~~~~~~~~~~~~~~End Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~Timesheet Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class timesheetWrap{
        public Timesheet_Detail__c timesheetDetail{get;set;}
        
        //Default constructor
        public timesheetWrap(){
            
        }
        
        //constructor with two param
        public timesheetWrap(Timesheet_Detail__c timesheetDetail){
            this.timesheetDetail = timesheetDetail;
        }
         
    }//end timesheetWrap
    
    //~~~~~~~~~~~~~~~~~~~~End Nested class~~~~~~~~~~~~~~~~~

    //~~~~~~~~~~~~~~~~~~~~~~~~Nested class~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public class ProjectWrap{
        public Project__c project_WP{get;set;}
       
        //Default constructor
        public ProjectWrap(){
            
        }
        
        //constructor with two param
        public ProjectWrap(Project__c project){
            this.project_wp = project;
        }
         
    }//end timesheetWrap 
    
    //~~~~~~~~~~~~~~~~~~~~End Nested class~~~~~~~~~~~~~~~

    /*--------------------Nested Class-----------------*/
}//end contrller