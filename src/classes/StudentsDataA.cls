public class StudentsDataA {
    
    @AuraEnabled
    public Static  list<Student__c> getStudents(){
        return [select id,Name,Name__c,Roll_No__c from Student__c ];
    }
    
    
     @AuraEnabled
    public Static  list<Student__c> sort_Students(list<Student__c> StudentsData){
        System.debug('StudentsData '+StudentsData);
        update StudentsData;
        return [select id,Name,Name__c,Roll_No__c from Student__c ];
    }

}