public with sharing class ContactAuruHandler {
	@AuraEnabled
    public static List<WrapperClass> getcontact(){
        Integer count=1;
        List<WrapperClass> wrapperList = new List<WrapperClass>();
        for(Contact contacts :[SELECT Id, LastName, Email, Phone FROM Contact LIMIT 1000] ){
            WrapperClass wc = new WrapperClass();
            wc.count = count++;
            wc.con = contacts;
            wrapperList.add(wc);
        }
        system.debug(wrapperList);
        return wrapperList;
    }
}