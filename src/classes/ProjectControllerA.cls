public class ProjectControllerA{

    public PageReference Back() {
        PageReference pageRef = new PageReference('/apex/Timesheet_home_slds_view');
     return pageRef;
    }

    public Project__c projectCon {get;set;}

    public list<Project_Task__c> taskList{get;set;}
    public List<ProjectTaskWrap> taskWrap {get;set;}
    public Integer count{get;set;}
    public Id project_ID{get;set;}
    public Boolean isProjectSave{get;set;}
    public Boolean isTaskSave{get;set;}
    
    public ProjectControllerA(){
        projectCon = new Project__c();
        taskList = new List<Project_Task__c>();
        taskWrap = new List<ProjectTaskWrap>();
        ID id = ApexPages.currentPage().getParameters().get('id');
        count=0;
         ProjectTaskWrap pt = new ProjectTaskWrap(new Project_Task__c(),1);
         count = 1;
         taskWrap.add(pt);
        System.debug('======================Default call ==='+count);
           
    }
//-----------------------adding row in list---------------
    public void addRow(){
        if(!taskWrap.isEmpty() && taskWrap != null){
            count++;
            ProjectTaskWrap pt = new ProjectTaskWrap(new Project_Task__c(),count);
            taskWrap.add(pt);
        }else{
             ProjectTaskWrap pt = new ProjectTaskWrap(new Project_Task__c(),1);
             count = 1;
            taskWrap.add(pt);
        }
            System.debug('========================Values === List====='+ taskWrap);
            System.debug('========================Values size === List===='+ taskWrap.size());
            System.debug('=======================Values ====count'+ count);
            
       
    }

//........removing row from list ----------------------------

    public void removeRow(){
     Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
     System.debug('========================Index === List+'+ param);
     if(!taskWrap.isEmpty() && taskWrap != null){
        for(Integer i = taskWrap.size()-1 ; i >=0; i--){
            if(taskWrap[i].counter == param){
                    taskWrap.remove(i);
                    System.debug('===================Values'+ taskWrap);
                }
          }
          count--;
         }else{
            ProjectTaskWrap pt = new ProjectTaskWrap(new Project_Task__c(),1);
            taskWrap.add(pt);
            count = 1;
         }
         System.debug('=======================Values === List==='+ taskWrap);
          System.debug('========================Values size === List===='+ taskWrap.size());
         System.debug('============Values ====count'+ count);
    }
    
    
    public void taskSave(id project_ID){
        if(!taskWrap.isEmpty() && taskWrap != null){
            for(ProjectTaskWrap pt : taskWrap ){
                Project_Task__c p = new Project_Task__c();
                p = pt.projectTask;
                p.Projectid__c = project_ID;
                taskList.add(p);  
            }
            System.debug('=======================Task === List==='+ taskList);
        }
    }
    
    
    // Save Project inbformation in custome sobject
    public PageReference save(){   
        if(projectCon.name != null){
            insert projectCon;
        }        
        if(projectCon.id != null){
            isProjectSave = false;
            taskSave(projectCon.id);
            System.debug('======isProjectSave====='+isProjectSave);
            if(!taskList.isEmpty() && taskList != null){
                try{
                    upsert taskList;
                    isTaskSave= false;
                     System.debug('======isTaskSave====='+isTaskSave);
                }catch(DMLException e){
                    isTaskSave= true;       
                }
            }      
        }else{
            isProjectSave = true; 
            isTaskSave= true;
        }
        System.debug('========Project ID=====Save== : '+ projectCon.id );
        PageReference pageRef = new PageReference('/apex/projectDetail?ID='+projectCon.ID);
        return pageRef;
    }
    
    public PageReference cancel(){
         PageReference pr = new PageReference('/apex/Timesheet_home_slds_view');
         return pr;
    }

    /*----------------Wrapper class-----------------------*/
    public class ProjectTaskWrap{
        
        //---Object and variable------
        public Project_Task__c projectTask{get;set;}
        public Integer counter {get;set;}

        public ProjectTaskWrap(){}

        public ProjectTaskWrap(Project_Task__c projectTask, Integer counter){
            this.projectTask = projectTask;
            this.counter= counter;
        }

    }
    /*----------------End Wrapper class-----------------------*/
}